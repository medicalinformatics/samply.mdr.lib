/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.lib;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A permissible value with all definitions available
 * for this permissible value.
 * @author paul
 *
 */
public class AdapterPermissibleValue implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The permissible value as string.
     */
    private String value = "";

    /**
     * The list of definitions for <b>this</b> permissible value.
     */
    private List<AdapterDefinition> definitions = new ArrayList<AdapterDefinition>();

    /**
     * The element Id
     */
    private int id = 0;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<AdapterDefinition> getDefinitions() {
        return definitions;
    }

    public void setDefinitions(List<AdapterDefinition> definitions) {
        this.definitions = definitions;
    }

    public void remove(AdapterDefinition definition) {
        definitions.remove(definition);
    }

    public void addDefinition() {
        definitions.add(new AdapterDefinition());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
