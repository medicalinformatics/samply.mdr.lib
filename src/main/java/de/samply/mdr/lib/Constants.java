/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.lib;

/**
 * Some basic constants and enums that are used in this Application
 * @author paul
 *
 */
public class Constants {

    /**
     * Represents the source of meta data elements.
     */
    public enum Source {
        LOCALMDR, CADSR;
    }

    /**
     * Represents the select box of validation type in the
     * data element wizard.
     * @author paul
     *
     */
    public enum DatatypeField {
        LIST, INTEGER, FLOAT, BOOLEAN, STRING, DATE, DATETIME, TIME, CATALOG;

        public String getName() {
            return toString();
        }
    }

    /**
     * Represents the language of a definition.
     * @author paul
     *
     */
    public enum Language {
        EN, DE, FR, ES;

        public String getName() {
            return toString().toLowerCase();
        }
    }

    /**
     * Defines the representation of a time value.
     * @author paul
     *
     */
    public enum TimeRepresentation {
        LOCAL_TIME, HOURS_24, HOURS_12;

        public String getName() {
            return toString().toLowerCase();
        }

        public static TimeRepresentation parseValidationData(String input) {
            if(input.contains(";")) {
                input = input.split(";")[1];
            }

            if(input.startsWith("LOCAL_TIME")) {
                return LOCAL_TIME;
            }

            if(input.startsWith("HOURS_24")) {
                return HOURS_24;
            }

            if(input.startsWith("HOURS_12")) {
                return HOURS_12;
            }

            return LOCAL_TIME;
        }
    }

    /**
     * Defines the representation of a date value.
     * @author paul
     *
     */
    public enum DateRepresentation {
        LOCAL_DATE, ISO_8601, DIN_5008;

        public String getName() {
            return toString().toLowerCase();
        }

        public static DateRepresentation parseValidationData(String input) {
            if(input.contains(";")) {
                input = input.split(";")[0];
            }

            if(input.startsWith("LOCAL_DATE")) {
                return LOCAL_DATE;
            }

            if(input.startsWith("ISO_8601")) {
                return ISO_8601;
            }

            if(input.startsWith("DIN_5008")) {
                return DIN_5008;
            }

            return LOCAL_DATE;
        }
    }

}
