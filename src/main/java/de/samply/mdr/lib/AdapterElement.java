package de.samply.mdr.lib;

import java.util.List;

/**
 *
 */
public interface AdapterElement {

    /**
     * Returns the identification for this element, e.g. "cadsr://dataelement?publicId=348203&version=1.0
     */
    public String getIdentification();

    /**
     * Returns the value domain for this element
     */
    public AdapterValueDomain createValueDomain();

    /**
     * Returns the definitions for this element.
     */
    public List<AdapterDefinition> getDefinitions();

}
