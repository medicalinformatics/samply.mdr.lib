/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.lib;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.samply.mdr.lib.Constants.DatatypeField;
import de.samply.mdr.lib.Constants.DateRepresentation;
import de.samply.mdr.lib.Constants.TimeRepresentation;

/**
 * The internal value domain. This class combines many different value domains, e.g.
 * a value domain with an integer range or a value domain with a list of
 * permissible values.
 * @author paul
 *
 */
public class AdapterValueDomain implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The list of permissible values.
     */
    protected List<AdapterPermissibleValue> permittedValues = new ArrayList<AdapterPermissibleValue>();

    /**
     * The deprecated format from ISO-11179
     */
    protected String format = "";

    /**
     * The datatype enum.
     */
    protected DatatypeField datatype = DatatypeField.INTEGER;

    /**
     * The unit of measure. Only relevant for integers and floats.
     */
    protected String unitOfMeasure = "";

    /**
     * If true, the integer or float must be within a range
     */
    protected Boolean withinRange = true;

    /**
     * If true, the lower/upper bound is enabled
     */
    protected Boolean useRangeFrom = true, useRangeTo = true;

    /**
     * The lower bound for integer and floats
     */
    protected Number rangeFrom = 0;

    /**
     * The upper bound for integer and floats.
     */
    protected Number rangeTo = 100;

    /**
     * If true, the day/seconds is also a part of the date/datetime/time.
     */
    protected Boolean withDays = true, withSeconds = true;

    /**
     * If true, a regular expression is used for validation.
     */
    protected Boolean useRegex = true;

    /**
     * The maximum characters
     */
    protected int maxLength = 0;

    /**
     * If true, the maxLength attribute is used.
     */
    protected Boolean useMaxLength = false;

    /**
     * The date representation.
     */
    protected DateRepresentation dateRepresentation = DateRepresentation.LOCAL_DATE;

    /**
     * The time representation.
     */
    protected TimeRepresentation timeRepresentation = TimeRepresentation.LOCAL_TIME;

    public void remove(AdapterPermissibleValue value) {
        permittedValues.remove(value);
    }

    public Boolean getUseRegex() {
        return useRegex;
    }

    public void setUseRegex(Boolean useRegex) {
        this.useRegex = useRegex;
    }

    public Boolean getWithDays() {
        return withDays;
    }

    public void setWithDays(Boolean withDays) {
        this.withDays = withDays;
    }

    public Boolean getWithSeconds() {
        return withSeconds;
    }

    public void setWithSeconds(Boolean withSeconds) {
        this.withSeconds = withSeconds;
    }

    public Number getRangeTo() {
        return rangeTo;
    }

    public void setRangeTo(Number rangeTo) {
        this.rangeTo = rangeTo;
    }

    public Number getRangeFrom() {
        return rangeFrom;
    }

    public void setRangeFrom(Number rangeFrom) {
        this.rangeFrom = rangeFrom;
    }

    public Boolean getUseRangeFrom() {
        return useRangeFrom;
    }

    public void setUseRangeFrom(Boolean useRangeFrom) {
        this.useRangeFrom = useRangeFrom;
    }

    public Boolean getUseRangeTo() {
        return useRangeTo;
    }

    public void setUseRangeTo(Boolean useRangeTo) {
        this.useRangeTo = useRangeTo;
    }

    public Boolean getWithinRange() {
        return withinRange;
    }

    public void setWithinRange(Boolean withinRange) {
        this.withinRange = withinRange;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public DatatypeField getDatatype() {
        return datatype;
    }

    public void setDatatype(DatatypeField datatype) {
        this.datatype = datatype;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public List<AdapterPermissibleValue> getPermittedValues() {
        return permittedValues;
    }

    public void setPermittedValues(List<AdapterPermissibleValue> permittedValues) {
        this.permittedValues = permittedValues;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public Boolean getUseMaxLength() {
        return useMaxLength;
    }

    public void setUseMaxLength(Boolean useMaxLength) {
        this.useMaxLength = useMaxLength;
    }

    public DateRepresentation getDateRepresentation() {
        return dateRepresentation;
    }

    public void setDateRepresentation(DateRepresentation dateRepresentation) {
        this.dateRepresentation = dateRepresentation;
    }

    public TimeRepresentation getTimeRepresentation() {
        return timeRepresentation;
    }

    public void setTimeRepresentation(TimeRepresentation timeRepresentation) {
        this.timeRepresentation = timeRepresentation;
    }

}
